const path = require('path')
module.exports ={
    env:{
        "BASE_URL":"http://localhost:3000",
        "MONGODB_URL":"mongodb+srv://admin:Devs@321$@cluster0.yarox.mongodb.net/mongodblearning?retryWrites=true&w=majority",
        "ACCESS_TOKEN_SECRET": "YOUR_ACCESS_TOKEN_SECRET",
        "REFRESH_TOKEN_SECRET": "YOUR_REFRESH_TOKEN_SECRET",
        "PAYPAL_CLIENT_ID": "YOUR_PAYPAL_CLIENT_ID",
        "CLOUD_UPDATE_PRESET": "YOUR_CLOUD_UPDATE_PRESET",
        "CLOUD_NAME": "YOUR_CLOUD_NAME",
        "CLOUD_API": "YOUR_CLOUD_API"
    },
    webpack:config =>{
        config.resolve.alias['components'] = path.join(__dirname,'components')
        config.resolve.alias['public'] = path.join(__dirname,'public')
        return config
    }
}