import Layout from 'components/Layout'
import { DataProvider } from '../Store/GlobalState';
import '../styles/globals.css'
export const config = {
  unstable_runtimeJS: false,
};
function MyApp({ Component, pageProps }) {
  return (
    <DataProvider>
    <Layout>
  <Component {...pageProps} />
  </Layout>
  </DataProvider>
)}

export default MyApp;
