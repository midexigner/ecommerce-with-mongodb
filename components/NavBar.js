import React from 'react'
import Link from 'next/link'
import {useRouter} from 'next/router'

const NavBar = () => {
  const router = useRouter()
  const isActive = (r) =>{
    if(r === router.pathname){
      return "active"
    }else{
      return ""
    }
  }
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container">
    <a className="navbar-brand" href="/">Ecommerce</a>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon" />
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <Link href="/cart"><a className={"nav-link " + isActive('/cart')} aria-current="page" ><i className="fas fa-shopping-cart" aria-hidden="true"></i> Cart</a></Link>
        </li>
        <li className="nav-item">
          <Link href="/signin"><a className={"nav-link " + isActive('/signin')} aria-current="page" ><i className="fas fa-user" aria-hidden="true"></i> Sign In</a></Link>
        </li>
       
        
      </ul>
     <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
     <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a className="dropdown-item" href="#">Action</a></li>
            <li><a className="dropdown-item" href="#">Another action</a></li>
            <li><hr className="dropdown-divider" /></li>
            <li><a className="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
       </ul>        
    </div>
  </div>
</nav>

    )
}

export default NavBar
